#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="pgp_mime_lib",
    version="0.1.4",
    description="Create PGP/MIME emails.",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai/pgp-mime-lib.git",
    install_requires=[
        "python-gnupg",
    ],
    packages=find_packages(),
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "gpgmime = pgp_mime_lib.tool:main",
        ],
    },
)
