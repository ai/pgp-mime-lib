import os
import shutil
import tempfile
import unittest
import smtplib

from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pgp_mime_lib

import logging
logging.getLogger('gnupg').setLevel(logging.DEBUG)
logging.basicConfig()


fixtures_dir = os.path.join(os.path.dirname(__file__), 'fixtures')
secret_key_dir = os.path.join(fixtures_dir, "gnupghome/private-keys-v1.d")


class TestBase(unittest.TestCase):

    def setUp(self):
        self.dir = tempfile.mkdtemp()

        # Load our test secret key.
        self.public_keyring = os.path.join(
            fixtures_dir, 'gnupghome/pubring.gpg')
        self.secret_key_dir = secret_key_dir
        self.secret_key = '0x56A1E35992BCB5CF'

    def tearDown(self):
        shutil.rmtree(self.dir)

    def _gpg(self):
        return pgp_mime_lib.GPG(self.secret_key, self.public_keyring, self.secret_key_dir)


class TestSign(TestBase):

    def test_sign_ok(self):
        result = self._gpg().sign_message(MIMEText('test message'))
        self.assertTrue(result)


class TestEncrypt(TestBase):

    def test_encrypt_and_sign_ok(self):
        with open(os.path.join(fixtures_dir, 'pubkey1.asc')) as fd:
            pubkey = fd.read()
        msg = MIMEText('test message')
        result = self._gpg().sign_and_encrypt_message(msg, pubkey)
        self.assertTrue(result)

    def test_encrypt_and_sign_ok_v2key(self):
        with open(os.path.join(fixtures_dir, 'pubkey2.asc')) as fd:
            pubkey = fd.read()
        msg = MIMEText('test message')
        result = self._gpg().sign_and_encrypt_message(msg, pubkey)
        self.assertTrue(result)

    def test_encrypt_fails_with_revoked_public_key(self):
        with open(os.path.join(fixtures_dir, 'revoked.asc')) as fd:
            pubkey = fd.read()
        self.assertRaises(
            pgp_mime_lib.Error,
            self._gpg().encrypt_message, MIMEText('test_message'), pubkey)

    def test_encrypt_fails_with_more_than_one_public_key(self):
        with open(os.path.join(fixtures_dir, 'twokeys.asc')) as fd:
            pubkey = fd.read()
        self.assertRaises(
            pgp_mime_lib.Error,
            self._gpg().encrypt_message, MIMEText('test_message'), pubkey)

    def test_encrypt_fails_with_secret_key(self):
        # People will do this!
        with open(os.path.join(fixtures_dir, 'secretkey.asc')) as fd:
            pubkey = fd.read()
        self.assertRaises(
            pgp_mime_lib.Error,
            self._gpg().encrypt_message, MIMEText('test_message'), pubkey)

    def test_encrypt_fails_with_bad_public_key(self):
        self.assertRaises(
            pgp_mime_lib.Error,
            self._gpg().encrypt_message, MIMEText('test_message'),
            'this is not a pgp key')

    def test_encrypt_fails_with_our_own_public_key(self):
        pubkey = '\n'
        self.assertRaises(
            pgp_mime_lib.Error,
            self._gpg().encrypt_message, MIMEText('test_message'),
            pubkey)

    def test_encrypt_and_sign_send_via_smtp(self):
        # Send an email to yourself if you want to manually check the
        # results, by defining some environment variables.
        try:
            smtp_server = os.environ['SMTP_SERVER']
            smtp_user = os.environ['SMTP_USER']
            smtp_password = os.environ['SMTP_PASSWORD']
            smtp_recip = os.environ['SMTP_RECIPIENT']
            smtp_pubkey = os.environ['SMTP_PUBLIC_KEY']
        except KeyError:
            raise unittest.SkipTest()

        with open(smtp_pubkey) as fd:
            pubkey = fd.read()
        msg = MIMEText('test message')
        result = self._gpg().sign_and_encrypt_message(msg, pubkey)
        result['To'] = smtp_recip
        result['Subject'] = 'test output'

        s = smtplib.SMTP_SSL(smtp_server, 465)
        s.login(smtp_user, smtp_password)
        s.sendmail(smtp_recip, [smtp_recip], result.as_string(unixfrom=False))
        s.quit()


class TestParsePublicKey(TestBase):

    def test_parse_public_key_ok(self):
        with open(os.path.join(fixtures_dir, 'pubkey1.asc')) as fd:
            pubkey = fd.read()
        result = pgp_mime_lib.parse_public_key(pubkey)
        self.assertTrue(result)

    def test_parse_public_key_fails_with_bad_public_key(self):
        self.assertRaises(
            pgp_mime_lib.Error, pgp_mime_lib.parse_public_key,
            'this is not a pgp key')

    def test_parse_public_key_fails_with_empty_data(self):
        self.assertRaises(
            pgp_mime_lib.Error, pgp_mime_lib.parse_public_key,
            '')

    def test_parse_public_key_fails_with_empty_pgp_key_block(self):
        self.assertRaises(
            pgp_mime_lib.Error, pgp_mime_lib.parse_public_key,
            '''
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2

-----END PGP PUBLIC KEY BLOCK-----
            ''')

    def test_parse_public_key_fails_with_revoked_public_key(self):
        with open(os.path.join(fixtures_dir, 'revoked.asc')) as fd:
            pubkey = fd.read()
        self.assertRaises(
            pgp_mime_lib.Error, pgp_mime_lib.parse_public_key,
            pubkey)

    def test_parse_public_key_fails_with_more_than_one_key(self):
        with open(os.path.join(fixtures_dir, 'twokeys.asc')) as fd:
            pubkey = fd.read()
        self.assertRaises(
            pgp_mime_lib.Error, pgp_mime_lib.parse_public_key,
            pubkey)

    def test_parse_public_key_fails_with_secret_key(self):
        with open(os.path.join(fixtures_dir, 'secretkey.asc')) as fd:
            pubkey = fd.read()
        self.assertRaises(
            pgp_mime_lib.Error, pgp_mime_lib.parse_public_key,
            pubkey)


class TestMIME(TestBase):

    def _encrypt(self, msg):
        with open(os.path.join(fixtures_dir, 'pubkey1.asc')) as fd:
            pubkey = fd.read()
        result = self._gpg().sign_and_encrypt_message(msg, pubkey)
        self.assertTrue(result)

    def test_simple_text_message(self):
        self._encrypt(MIMEText('test message'))

    def test_base64_text_message(self):
        self._encrypt(MIMEText(u'test message', 'plain', 'utf-8'))

    def test_8bit_text_message(self):
        m = MIMEText(u'test message')
        m.replace_header('Content-Transfer-Encoding', '8bit')
        self._encrypt(m)

    def test_multipart_message_with_image(self):
        msg = MIMEMultipart()
        msg.preamble = 'This is a message with an image attachment'
        msg.attach(MIMEImage('GIF89ZZ', 'gif'))
        self._encrypt(msg)

    def test_multipart_alternative(self):
        msg = MIMEMultipart('alternative')
        msg.attach(MIMEText('text version', 'plain'))
        msg.attach(MIMEText('html version', 'html'))
        self._encrypt(msg)
