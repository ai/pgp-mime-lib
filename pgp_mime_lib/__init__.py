# Package to send PGP-encrypted emails to users using ephemeral key
# storage (so, no keyserver interaction, no keyring management).
from __future__ import absolute_import

import contextlib
import os
import re
import shutil
import tempfile

from email import encoders
from email.mime.application import MIMEApplication
from email.generator import Generator

import gnupg

from ._compat import StringIO
from . import rfc3156


# Digest algorithm to use in signatures.
DIGEST_ALGO = 'SHA256'


class Error(Exception):
    pass


class GPG(object):
    """GPG encrypted email generator.

    Uses a specific key from a secret keyring, but it does not
    maintain a public keyring. Instead, it is given the ASCII-encoded
    public key for every recipient, and it imports it to a temporary
    public keyring which is deleted afterwards.

    """

    def __init__(self, key_id, public_keyring=None, secret_key_dir=None,
                 passphrase=None):
        if not public_keyring:
            public_keyring = os.path.join(_default_gpghome(), 'pubring.kbx')
        if not secret_key_dir:
            secret_key_dir = os.path.join(_default_gpghome(), 'private-keys-v1.d')

        for f in (public_keyring, secret_key_dir):
            if not os.path.exists(f):
                raise Error('no such file or directory: %s' % f)

        self.public_keyring = public_keyring
        self.secret_key_dir = secret_key_dir
        self.secret_key_id = key_id
        self.passphrase = passphrase

    @contextlib.contextmanager
    def _sane_gpg(self, public_keyring=None):
        """Create a temporary GPG environment.

        If public_keyring is None, the temporary environment will
        contain a writable public keyring, that will be a copy of the
        default one. If we don't need to import keys, we can pass
        self.public_keyring as an optimization (avoids a file copy).

        Note that we must always have our public key available for
        signing.

        """
        with _temp_dir() as tmpdir:
            if not public_keyring:
                public_keyring = os.path.join(tmpdir, 'pubring.gpg')
                shutil.copy(self.public_keyring, public_keyring)
                os.chmod(public_keyring, 0o600)

            # Create a gpg.conf with some options. Not all are used
            # and for some of them there are equivalent command-line
            # options available through the gnupg module.
            with open(os.path.join(tmpdir, 'gpg.conf'), 'w') as fd:
                fd.write('''
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
cert-digest-algo SHA256
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed
sig-notation issuer-fpr@notations.openpgp.fifthhorseman.net=%%g
throw-keyids
default-key %s\n
''' % self.secret_key_id)  # noqa

            # symlink the private key directory
            os.symlink(self.secret_key_dir, os.path.join(tmpdir, 'private-keys-v1.d'))

            # Create the gnupg.GPG object to talk to gnupg.
            gpg = gnupg.GPG(gpgbinary=_default_gpgbinary(),
                            gnupghome=tmpdir,
                            keyring=public_keyring)
            # XXX this below might be wrong.
            gpg.encoding = 'utf-8'
            yield gpg

    def _encrypt(self, gpg, msg_str, public_key):
        """Encrypt msg_str to the given public_key."""
        # Import the key (or keys) provided to us into a temporary
        # keyring. We would really like to call gpg with the
        # --hidden-recipients-file option, to avoid importing the
        # public key, but we can't do it with the gnupg module.
        fp = _import_one_key(gpg, public_key)
        s = gpg.encrypt(msg_str, fp, passphrase=self.passphrase,
                        always_trust=True, extra_args=["--throw-keyids"])
        if not s:
            raise Error('gpg --encrypt failed (no result)')
        return str(s)

    def _sign(self, gpg, msg_str):
        """Sign msg_str and return the detached signature."""
        s = gpg.sign(msg_str, clearsign=False, detach=True, binary=False,
                     passphrase=self.passphrase)
        if not s:
            raise Error('gpg --sign failed (no result)')
        if not s.data:
            raise Error('gpg --sign failed (no result data)')
        return s.data

    def _encrypt_message(self, gpg, msg, public_key):
        """Build a multipart/encrypted MIME message."""
        container = rfc3156.MultipartEncrypted('application/pgp-encrypted')

        enc_msg = MIMEApplication(
            self._encrypt(gpg, msg.as_string(unixfrom=False), public_key),
            _subtype='octet-stream',
            _encoder=encoders.encode_noop)
        enc_msg.add_header('Content-Disposition', 'attachment',
                           filename='msg.asc')

        meta_msg = rfc3156.PGPEncrypted()
        meta_msg.add_header('Content-Disposition', 'attachment')

        container.attach(meta_msg)
        container.attach(enc_msg)

        return container

    def _sign_message(self, gpg, msg):
        """Build a multipart/signed MIME message."""
        container = rfc3156.MultipartSigned('application/pgp-signature',
                                            'pgp-' + DIGEST_ALGO.lower())

        # _openpgp_mangle_for_signature modifies msg as a side effect.
        sig_msg = rfc3156.PGPSignature(
            self._sign(gpg, _openpgp_mangle_for_signature(msg)))

        container.attach(msg)
        container.attach(sig_msg)
        return container

    def encrypt_message(self, msg, public_key):
        """Encrypt msg to public_key.

        Args:
          msg (email.Message) message to sign
          public_key (str) public key of the recipient
        Returns:
          An email.Message object.
        """
        with self._sane_gpg() as gpg:
            return self._encrypt_message(gpg, msg, public_key)

    def sign_and_encrypt_message(self, msg, public_key):
        """Sign and encrypt a message to public_key.

        Args:
          msg (email.Message) message to sign
          public_key (str) public key of the recipient
        Returns:
          An email.Message object.
        """
        with self._sane_gpg() as gpg:
            return self._encrypt_message(gpg, self._sign_message(gpg, msg), public_key)

    def sign_message(self, msg):
        """Sign the given message.

        Args:
          msg (email.Message) message to sign
        Returns:
          An email.Message object.
        """
        with self._sane_gpg(self.public_keyring) as gpg:
            return self._sign_message(gpg, msg)


def _openpgp_mangle_for_signature(msg):
    """Return a message suitable for signing.

    Encodes multipart message parts in msg as base64, then renders the
    message to string enforcing the right newline conventions. The
    returned value is suitable for signing according to RFC 3156.

    The incoming message is modified in-place.

    """
    rfc3156.encode_base64_rec(msg)
    fp = StringIO()
    g = Generator(
        fp, mangle_from_=False, maxheaderlen=76)
    g.flatten(msg)
    s = re.sub('\r?\n', '\r\n', fp.getvalue())
    if msg.is_multipart():
        if not s.endswith('\r\n'):
            s += '\r\n'
    return s


def _default_gpghome():
    if 'GNUPGHOME' in os.environ:
        return os.environ['GNUPGHOME']
    return os.path.expanduser('~/.gnupg')


def _default_gpgbinary():
    return os.getenv('GNUPG', 'gpg')


@contextlib.contextmanager
def _temp_dir():
    """Create a temporary directory, as a context manager."""
    tmpdir = tempfile.mkdtemp()
    try:
        yield tmpdir
    finally:
        shutil.rmtree(tmpdir, ignore_errors=True)


def parse_public_key(public_key):
    """Parses a PGP public key.

    The input is expected to contain a single PGP public key, either
    in ASCII-armored format or not. The function will return the key
    fingerprint, or raise an Error (possibly with a descriptive
    message) if there was an error.

    """
    with _temp_dir() as tmpdir:
        # Create empty pubrings.
        for p in 'pubring.gpg', 'secring.gpg':
            with open(os.path.join(tmpdir, p), 'w'):
                pass
        gpg = gnupg.GPG(gpgbinary=_default_gpgbinary(),
                        gnupghome=tmpdir)
        gpg.encoding = 'utf-8'
        return _import_one_key(gpg, public_key)


def _import_one_key(gpg, public_key):
    """Import a single PGP key and return its fingerprint."""
    import_result = gpg.import_keys(public_key)
    if not import_result:
        raise Error('Generic GPG import error')

    # Error handling is conservative: we are looking for exactly
    # one new key. We also want to report meaningful error
    # messages, so we are just going to bail out on the first bad
    # status returned by gpg.
    if import_result.count != 1:
        raise Error('more than one public key found in input')

    fp = None
    for res in import_result.results:
        # "Not actually changed" in those cases, such as testing,
        # where the public key is already in our keyring.
        if ('Entirely new key' not in res['text']) and ('Not actually changed' not in res['text']):
            raise Error('no valid keys found in input: %r' % res['text'])
        if not res['fingerprint']:
            continue
        if fp:
            raise Error('more than one public key found in input')
        fp = res['fingerprint']
    if not fp:
        raise Error('no valid keys found in input')

    return fp
