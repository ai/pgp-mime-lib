import argparse
import email.parser
import sys

from . import GPG


PRESERVED_HEADERS = [
    'From',
    'To',
    'Cc',
    'Bcc',
    'Subject',
    'Errors-To',
    'Reply-To',
    'Message-Id',
]


def main():
    parser = argparse.ArgumentParser(
        description='Sign an email message with PGP/MIME.')
    parser.add_argument(
        '--keyid', required=True,
        help='PGP key ID used for the signature')
    parser.add_argument(
        'input_message', nargs='?', type=argparse.FileType('rb'),
        default=sys.stdin.buffer)
    args = parser.parse_args()

    msg = email.parser.BytesParser().parse(args.input_message)
    saved_hdrs = dict((x, msg[x]) for x in PRESERVED_HEADERS)
    pgp = GPG(args.keyid)
    msg = pgp.sign_message(msg)
    for k, v in saved_hdrs.items():
        if v:
            msg[k] = v
    print(msg.as_string())


if __name__ == '__main__':
    main()
