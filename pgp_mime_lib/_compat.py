import sys
import base64


PY2 = sys.version_info[0] == 2


if not PY2:
    from io import StringIO
    string_types = (str,)
else:
    from cStringIO import StringIO
    string_types = (str, unicode)


def base64_encodestring(data):
    if PY2:
        return base64.encodestring(data)

    return base64.encodebytes(data)
