pgp-mime-lib
============

Simple Python package to send
[PGP/MIME](https://www.ietf.org/rfc/rfc3156.txt) multipart/encrypted
and multipart/signed emails. Uses the
[python-gnupg](https://gnupg.readthedocs.io/en/latest/) Python module to talk to
gpg.

The package is agnostic with respect to key management: recipients'
public keys are not permanently stored in a keyring, but are passed
along with each message by the caller.

Python compatibility: 2.7 / 3.5+
